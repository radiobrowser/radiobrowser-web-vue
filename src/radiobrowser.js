/* eslint-disable */
/*
It is not possible to do a reverse DNS from a browser yet.
The first part (a normal dns resolve) could be done from a browser by doing DOH (DNS over HTTPs)
to one of the providers out there. (google, quad9,...)
So we have to fallback to ask a single server for a list.
*/

let serverlist = [];

/**
 * Shuffle array
 */
function shuffle(array) {
    var currentIndex = array.length,
        temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

/**
 * Ask a specified server for a list of all other server.
 */
function get_radiobrowser_base_urls() {
    return new Promise((resolve, reject) => {
        var request = new XMLHttpRequest()
        // If you need https, please use the fixed server fr1.api.radio-browser.info for this request only
        request.open('GET', 'https://fr1.api.radio-browser.info/json/servers', true);
        request.onload = function () {
            if (request.status >= 200 && request.status < 300) {
                var items = JSON.parse(request.responseText).map(x => "https://" + x.name);
                let servers = [];
                for (let item of items) {
                    if (!servers.includes(item)) {
                        servers.push(item);
                    }
                }
                shuffle(servers);
                resolve(servers);
            } else {
                reject(request.statusText);
            }
        }
        request.send();
    });
}

/**
 * Ask a server for its settings.
 */
function get_radiobrowser_server_config(baseurl) {
    return new Promise((resolve, reject) => {
        var request = new XMLHttpRequest()
        request.open('GET', baseurl + '/json/config', true);
        request.onload = function () {
            if (request.status >= 200 && request.status < 300) {
                var items = JSON.parse(request.responseText);
                resolve(items);
            } else {
                reject(request.statusText);
            }
        }
        request.send();
    });
}

/**
 * Do request with to server, supports GET and POST with JSON
 */
function rq(method, basepath, relativepath, data) {
    return new Promise((resolve, reject) => {
        var request = new XMLHttpRequest()
        request.open(method, basepath + "/" + relativepath, true);
        if (data) {
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        }
        request.onload = function () {
            if (request.status >= 200 && request.status < 300) {
                resolve(JSON.parse(request.responseText));
            } else {
                reject(request.statusText);
            }
        }
        request.send(JSON.stringify(data));
    });
}

/**
 * Do request do radiobrowser server, choose random one, fallback to others
 */
export async function rb_rq(method, relativepath, data) {
    // refresh list if not defined
    if (!serverlist || serverlist.length === 0) {
        serverlist = await get_radiobrowser_base_urls();
    }
    for (let i = serverlist.length - 1; i >= 0; i--) {
        let server = serverlist[i];
        try {
            let result = await rq(method, server, relativepath, data);
            return result;
        } catch (err) {
            serverlist.length = serverlist.length - 1;
            console.log("Error:", err);
        }
    }
    throw new Error("Unable to connect to any radiobrowser server");
}

/**
 * Count click
 */
export async function rb_click(uuid) {
    return rb_rq("GET", `json/url/${uuid}`);
}

/**
 * Get changes of a stream by uuid
 */
export function get_changes(uuid) {
    return rb_rq("GET", 'json/stations/changed/' + uuid).then(function (list_changes) {
        for (let i = 0; i < list_changes.length; i++) {
            list_changes[i].lastchangetime = new Date(list_changes[i].lastchangetime.replace(" ", "T") + "Z");
            list_changes[i].lastchangetime_locale = new Date(list_changes[i].lastchangetime).toLocaleString();
        }
        return list_changes;
    });
}

export function get_clicks(uuid, seconds) {
    return rb_rq("GET", 'json/clicks/' + uuid + "?seconds=" + seconds).then(function (list_clicks) {
        for (var i = 0; i < list_clicks.length; i++) {
            list_clicks[i].clicktimestamp = new Date(list_clicks[i].clicktimestamp.replace(" ", "T") + "Z");
            list_clicks[i].clicktimestamp_locale = new Date(list_clicks[i].clicktimestamp).toLocaleString();
        }
        return list_clicks;
    });
}

export function get_checks(uuid) {
    return rb_rq("GET", 'json/checks/' + uuid).then(function (list_checks) {
        for (var i = 0; i < list_checks.length; i++) {
            list_checks[i].timestamp = new Date(list_checks[i].timestamp.replace(" ", "T") + "Z");
            list_checks[i].timestamp_locale = new Date(list_checks[i].timestamp).toLocaleString();
        }
        return list_checks;
    });
}