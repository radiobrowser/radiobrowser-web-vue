/* eslint-disable */
import {
    Howl,
    Howler
} from "howler";

// global player object
let player = null;

// play stream with url
export function play(url) {
    if (Howler) {
        if (player) {
            player.stop();
        }
        player = new Howl({
            src: url,
            html5: true
        });
        player.play();
    }
}